
public class Controlador {

	Controlador(){
		
		Automovil a = new Automovil();
		
		/* valor por defecto */
		String cilindrada = "1.2";
		
		Motor m = new Motor(cilindrada);
		
		a.setMotor(m);
		
		Velocimetro v = new Velocimetro();
		a.setVelocimetro(v);
		
		Estanque e = new Estanque();
		a.setEstanque(e);
		
		for (int i=0; i<4;i++) {
			a.setRueda(new Rueda());
		}
		
		//a.chequeo_ruedas();
		a.reporte_automovil();
		
	}
}

