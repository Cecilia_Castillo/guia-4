import java.util.ArrayList;

public class Automovil {

	private Motor motor;
	private Velocimetro velocimetro;
	private ArrayList<Rueda> rueda;
	private Estanque estanque;
	private Boolean encendido ;
	
	Automovil(){
		this.rueda = new ArrayList<Rueda>();
		this.encendido = false;
	}
	
	public Motor getMotor() {
		return motor;
	}
	
	public void setMotor(Motor motor) {
		this.motor = motor;
	}

	public Velocimetro getVelocimetro() {
		return velocimetro;
	}

	public void setVelocimetro(Velocimetro velocimetro) {
		this.velocimetro = velocimetro;
	}

	public Estanque getEstanque() {
		return estanque;
	}

	public void setEstanque(Estanque estanque) {
		this.estanque = estanque;
	}

	public Boolean getEncendido() {
		return encendido;
	}

	public void setEncendido(Boolean encendido) {
		this.encendido = encendido;
	}

	public ArrayList<Rueda> getRueda() {
		return rueda;
	}

	public void setRueda(Rueda rueda) {
		this.rueda.add(rueda);
	}
	
	public void enciende_automovil() {
		
		this.encendido = true;
		
	}
	
	public void movimiento() {
	
		System.out.println("El auto se mueve");
	}
	
	public void chequeo_ruedas() {
	
		for (Rueda r : this.rueda) {
			System.out.println(r.getDesgaste());
		}
	}
	
	public void reporte_automovil() {
		System.out.println("El automovil tiene un motor de " + this.motor.getCilindrada());
		System.out.println("El estado actual del estanque es: " + this.estanque.getVolumen());
		chequeo_ruedas();
		System.out.println("La velocidad actual del automovil es " + this.velocimetro.getVelocidad());
	}
	
}

